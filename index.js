/**
 * Bài 5
 */

function timSo() {
  var soN = document.getElementById("soN").value * 1;
  var soNguyento = "";
  if (soN < 2) {
    document.getElementById("soNguyento").innerText = "Không có số nguyên tố";
  } else {
    for (var i = 2; i <= soN; i++) {
      if (isNguyento(i)) {
        soNguyento += `${i}  `;
      }
    }
    document.getElementById("soNguyento").innerText = soNguyento;
  }
}

function isNguyento(x) {
  var flag = true;
  for (var i = 2; i < x - 1; i++) {
    if (x % i == 0) {
      flag = false;
      break;
    }
  }
  return flag;
}
